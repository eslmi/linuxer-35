#!/bin/bash
# Author 22组：吴旭东、李丞罡、张鹏
# Contact：2838215550@qq.com
# Data：2019-12-06

. /opt/linuxer/etc/server.conf
. /opt/linuxer/lib/server.lib

clear
ViewAccountMenu()
{
	MenuWord()
	{
		clear
		echo "View manager account functions."
		echo "MENU"
		echo "  1 View all teacher account"
		echo "  2 View lock teacher account"
		echo "  3 View unlock teacher account"
		echo "  4 Lock teacher account"
		echo "  5 Unlock teacher account"
		echo "  6 Delete teacher account"
		echo "  7 Teacher account change password" 
		echo "  8 Register teacher account"
		echo "  0 return"
		echo 
	}
	MenuWord
	while true
	do
		read -p "Please input your choice:" VCHOICE
		if [ -z $VCHOICE ];then
			VCHOICE=0
		fi
		case $VCHOICE in
			#查看所有账号
			1) GetAllTeacherAccount;;
			#查看被锁账号
			2) GetLockAccount;;
			#查看未锁账号
			3) GetUnlockAccount;;
			#教师账号加锁
			4) LockAccount;;
			#教师账号解锁
			5) UnlockAccount;;
			#教师账号删除
			6) DeleteAccount;;
			#教师账号修改密码
			7) ChangeAccountPasswd;;
			#增加教师账号
			8) RegisterTeacherAccount&&MenuWord;;
			*) VHOICE=0
		esac
		if [ $VCHOICE == 0 ];then
			return
		fi
	done
	
}


#CHOICE初始赋值为9
CHOICE=9
#清屏
clear

#生成教师连接服务器的菜单
#选项1更改密码
#选项2查看成绩列表
#选项3实验评分
#选项4实验分析
#选项5系统管理
#选项0退出
#当CHOICE值不为0的时候进入菜单循环
while [ ! $CHOICE -eq 0 ]
do
clear
ShowLockCount
echo 
echo "Manager functions."
echo "MENU"
echo "  1 View teacher account"
echo "  2 Lock teacher account"
echo "  3 Unlock teacher account"
echo "  4 Delete teacher account"
echo "  5 Teacher account change password" 
echo "  6 Register teacher account"
echo "  7 Change passwd"
echo "  8 List scores"
echo "  9 Mark reports"
echo "  a System admin" 
echo "  0 return"
echo "Please input your choice:"

read CHOICE

#根据选项调用对应的函数
case $CHOICE in
	#查看教师账号
  1) ViewAccountMenu;;
	#教师账号加锁
  2) LockAccount;;
	#教师账号解锁
  3) UnlockAccount;;
	#教师账号删除
  4) DeleteAccount;;
	#教师账号修改密码
  5) ChangeAccountPasswd;;
		#增加教师账号
  6) RegisterTeacherAccount;;
  7) passwd;;
  #列出分数
  8) listscore;;
  #打分
  9) mark;;
  a) system;;
	#退出
	#CHOICE赋值为0退出循环
  *) CHOICE=0
esac

done

