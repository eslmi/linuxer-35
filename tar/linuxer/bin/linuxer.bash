#!/bin/bash

#执行client.conf和client.lib脚本，用于定义变量和函数
#引入配置全局变量
. /opt/linuxer/etc/client.conf
#引入函数
. /opt/linuxer/lib/client.lib


#初始选项为9,保证正常进入系统
#设置初始值
CHOICE=9
#当CHOICE等于0就退出菜单
while [ ! $CHOICE -eq 0 ]
do

#生成学生客户端菜单
#选项1打开实验环境
#选项2提交实验结果
#选项3连接到linuxer服务器
#选项4退出

# by 5120170405
echo "
  _      _                           
 | |    (_)                          
 | |     _ _ __  _   ___  _____ _ __ 
 | |    | | '_ \| | | \ \/ / _ \ '__|
 | |____| | | | | |_| |>  <  __/ |   
 |______|_|_| |_|\__,_/_/\_\___|_|   
                                     
                                     "

#输出菜单
echo "This is the client of Linuxer for student. (V4.0)"
echo "MENU"
echo "  1 Start experiment environment (Press Ctl+c to stop)"
echo "  2 Submit your experiment result"
echo "  3 Connect to the server of linuxer"
echo "  0 EXIT"
echo "Please input your choice:"

#读取用户的选项
#读入用户输入选择参数
read CHOICE

#根据选项调用对应的函数
case $CHOICE in
	#调用start函数，第一个传入参数是实验资源包的路径，如/opt/linuxer/labs/
  1) start /opt/linuxer/labs/;;
	#调用submit函数，第一个传入参数是localhost
  2) submit ${1:-localhost};;
	#调用sever函数，第一个传入参数是localhost
  3) server ${1:-localhost};;
	#给CHOICE赋值为0，退出循环
  *) CHOICE=0
esac

done

