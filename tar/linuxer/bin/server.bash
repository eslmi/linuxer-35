#!/bin/bash
#加载服务器配置文件，以及相应函数
#引入设置全局变量
. /opt/linuxer/etc/server.conf

#引入函数
. /opt/linuxer/lib/server.lib


#赋初始值
CHOICE=9

#clear

#生成菜单
#print menu
#生成服务器的菜单
#选项1注册用户
#选项2服务器功能
#选项3退出
while [ ! $CHOICE -eq 0 ]
do

echo "This is the server of Linuxer. (V4.0)"
echo "MENU"
echo "  1 Register a student account"
echo "  2 Register a teacher account"
echo "  3 Functions" 
echo "  0 Return"
echo "Please input your choice:"

read CHOICE

#调用函数
#functions call
#根据选项调用对应的函数
case $CHOICE in
	#注册学生账号
  1) register;;
	#注册教师账号
  2) RegisterTeacherAccount;;
	#登陆账号
  3) (read -p "ID:" ID && su $ID);;
	#退出
  *) CHOICE=0
esac

done

