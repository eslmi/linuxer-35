#!/bin/bash
#导入需要调用的shell脚本
. /opt/linuxer/etc/client.conf
. /opt/linuxer/lib/client.lib
	
clear
#调用进度条函数
Bar
echo ""
echo -e "\033[45;37m This is the client of Linuxer for teacher. (V4.0) \033[0m"
##调用打印菜单函数
Func_View 2 "MENU"
Func_View 0 "1 Modify experiment environment (Press Ctl+c to stop)"
Func_View 0 "2 Submit experiment environment"
Func_View 0 "3 Export scores and reports"
Func_View 0 "4 Connect to the server of linuxer"
Func_View 0 "0 EXIT"
#设置t=1作为循环条件
t=1
read -p "please input your choice:" CHOICE
while [ $t = 1 ]
do
	case $CHOICE in
		0)
			Rotate_Cursor 1
			echo -e "\033[32m successful exit \033[0m "
			exit 0;;
		1)
			t=0
			start ;;
		2)
			t=0
			submit;;
		3)
			t=0;;
		4)
			t=0
			server;;
		*)
		#当用户输入错误时，调用旋转光标函数，设置时间为1秒，再调用打印菜单项目，输出错误提示，最后提示用户再次输入正确的选项
			Rotate_Cursor 1
        		Func_View 1 "input is wrong!"
        		read -p "please input your choice aright!:" CHOICE;;

	esac
done
