#!/bin/bash
#配置服务器学生端，以及函数的加载
. /opt/linuxer/etc/server.conf
. /opt/linuxer/lib/server.lib

#TERM=linux
#初始选项为9,保证正常进入系统
CHOICE=9
clear

#生成学生连接服务器的菜单
#选项1更改密码
#选项2查看成绩
#选项3退出
while [ ! $CHOICE -eq 0 ]
do

echo "Student function."
echo "MENU"
echo "  1 Change passwd"
echo "  2 Check score" 
echo "  3 Bash" 
echo "  0 Return"
echo "Please input your choice:"

read CHOICE

#根据选项调用对应的函数
case $CHOICE in
	#执行passwd命令（）函数
  1) passwd;;
	#执行查分数函数
  2) checkscore;;
  3) /bin/bash;;
	#退出
  *) CHOICE=0
esac

done

